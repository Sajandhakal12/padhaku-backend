const getFacetPipeline = (options = {}) => {
  return {
    $facet: {
      pagination: [
        { $count: 'total' },
        // eslint-disable-next-line radix
        { $addFields: { page: parseInt(options.page || 1) } },
      ],
      docs: [
        {
          $skip:
            // eslint-disable-next-line radix
            (parseInt(options.page || 1) - 1) * parseInt(options.limit || 10),
        },
        {
          // eslint-disable-next-line radix
          $limit: parseInt(options.limit || 10),
        },
      ],
    },
  };
};

module.exports = getFacetPipeline;
