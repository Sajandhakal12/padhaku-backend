class AppError extends Error {
  constructor(message, statusCode) {
    super(message);
    this.statusCode = statusCode;
    this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error';
    this.isOperational = true;

    // when new object is created and constructor is created the error will not be included in stack trace
    Error.captureStackTrace(this, this.constructor);
  }
}
module.exports = AppError;
