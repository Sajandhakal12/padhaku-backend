const mongoose = require('mongoose');

const directMessageSchema = new mongoose.Schema(
  {
    toUser: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    fromUser: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    type: {
      type: String,
      enum: ['file', 'message', 'reply'],
      default: 'message',
    },
    data: {
      type: String,
      required: true,
    },
    reply: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Message',
    },
  },
  { timestamps: true }
);
const DirectMessage = mongoose.model('DirectMessage', directMessageSchema);

module.exports = DirectMessage;
