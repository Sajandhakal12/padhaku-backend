const mongoose = require('mongoose');

const Thread = require('./thread.model');

const likeSchema = new mongoose.Schema(
  {
    thread: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Thread',
      required: true,
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
  },
  { timestamps: true }
);
likeSchema.index({ thread: 1, user: 1 }, { unique: true });

likeSchema.post('save', async function () {
  await Thread.updateOne({ _id: this.thread }, { $inc: { likeCount: 1 } });
});

const Like = mongoose.model('Like', likeSchema);

module.exports = Like;
