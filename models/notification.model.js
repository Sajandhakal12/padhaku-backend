const mongoose = require('mongoose');

const chatroomMessageSchema = new mongoose.Schema(
  {
    recipient: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    sender: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    type: {
      type: String,
      enum: ['like', 'comment', 'reply'],
      required: true,
    },
    in: {
      type: String,
      enum: ['DirectMessage', 'chatroom', 'Thread'],
    },
    seen: {
      type: String,
      default: false,
    },
    thread: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Thread',
    },
    message: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Message',
    },
    directMessage: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Message',
    },
  },
  { timestamps: true }
);
chatroomMessageSchema.pre(/^find/, function (next) {
  // this points to the current query
  this.populate({
    path: 'sender',
    select: 'firstName lastName imageUrl',
  });
  next();
});
const Notification = mongoose.model('Notification', chatroomMessageSchema);

module.exports = Notification;
