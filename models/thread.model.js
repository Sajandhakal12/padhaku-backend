const mongoose = require('mongoose');

const threadSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    body: {
      type: String,
      required: true,
    },
    tag: {
      type: [String],
      enum: [
        'myth',
        'romance',
        'novel',
        'scifi',
        'fantasy',
        'biography',
        'drama',
        'nonfiction',
        'selfhelp',
        'poem',
        'school',
        'college',
        'university',
      ],
    },
    likeCount: { type: Number, default: 0 },
    commentCount: { type: Number, default: 0 },
  },
  { timestamps: true }
);
const Thread = mongoose.model('Thread', threadSchema);

module.exports = Thread;
