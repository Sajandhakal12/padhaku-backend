const mongoose = require('mongoose');

const bookSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, 'Book must have a title'],
    },
    author: {
      type: String,
      required: [true, 'Book Must have a author'],
    },
    image: {
      type: String,
      // default: 'default.jpg',
    },
    publication: {
      type: String,
    },
    ratingsAverage: {
      type: Number,
      min: 0,
      max: 5,
    },
    ratingsQuantity: Number,
    genre: {
      type: [String],
      enum: [
        'myth',
        'romance',
        'novel',
        'scifi',
        'fantasy',
        'biography',
        'drama',
        'nonfiction',
        'selfhelp',
        'poem',
        'school',
        'college',
        'university',
      ],
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);
const Book = mongoose.model('Book', bookSchema);

module.exports = Book;
