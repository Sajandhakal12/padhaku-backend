const mongoose = require('mongoose');
const Thread = require('./thread.model');

const commentSchema = new mongoose.Schema(
  {
    body: {
      type: String,
      required: true,
    },
    thread: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Thread',
      required: true,
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    likeCount: { type: Number, default: 0 },
  },
  { timestamps: true }
);

commentSchema.post('save', async function () {
  await Thread.updateOne({ _id: this.thread }, { $inc: { commentCount: 1 } });
});

const Comment = mongoose.model('Comment', commentSchema);

module.exports = Comment;
