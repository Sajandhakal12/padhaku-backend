const mongoose = require('mongoose');

const userBookSchema = new mongoose.Schema(
  {
    book: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Book',
      required: [true, 'Ads must be linked to book present in the database'],
    },
    user: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: [true, 'User is required'],
    },
    price: {
      type: Number,
      required: true,
    },
    images: {
      type: [String],
    },
    condition: {
      type: String,
      required: true,
      enum: ['likenew', 'fair', 'poor'],
    },
    isNegotiable: {
      type: Boolean,
      required: true,
    },
    dateofbought: {
      type: Date,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    isSold: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

userBookSchema.pre(/^find/, function (next) {
  // this points to the current query
  this.populate({
    path: 'book',
    select: 'title author publication genre',
  });
  next();
});

const UserBook = mongoose.model('UserBook', userBookSchema);

module.exports = UserBook;
