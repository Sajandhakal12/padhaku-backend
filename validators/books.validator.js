const Yup = require('yup');

exports.bookSchemaValidation = Yup.object({
  body: Yup.object({
    title: Yup.string().required('Title is required'),
    author: Yup.string().required('Author is required'),
    publication: Yup.string().required('Publication is required'),
    genre: Yup.array()
      .of(
        Yup.string().oneOf([
          'myth',
          'romance',
          'novel',
          'scifi',
          'fantasy',
          'biography',
          'drama',
          'nonfiction',
          'selfhelp',
          'poem',
          'school',
          'college',
          'university',
        ])
      )
      .min(1, 'Must contain atleast one interest')
      .required('interest is required'),
    description: Yup.string().required('Description is required'),
    rating: Yup.string().matches(/[1-5]/, 'The rating must be between 1 to 5'),
    review: Yup.string(),
  })
    .noUnknown(true)
    .strict(),
});
