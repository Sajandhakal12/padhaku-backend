const Yup = require('yup');

exports.postUserBookValidation = Yup.object({
  body: Yup.object({
    book: Yup.string().required('Book Id is required'),
    user: Yup.string().required('User Id is required'),
    price: Yup.string()
      .matches(/^[0-9]*$/, 'Price must be a number')
      .required('Price is required'),
    condition: Yup.string()
      .oneOf(['likenew', 'fair', 'poor'])
      .required('Publication is required'),
    isNegotiable: Yup.string()
      .oneOf(['true', 'false'])
      .required('isNegotiable flag is required'),
    dateofbought: Yup.string().required('Bought date is required'),
    description: Yup.string().required('Description is required'),
  })
    .noUnknown(true)
    .strict(),
});
