const Yup = require('yup');

exports.regValidationSchema = Yup.object({
  body: Yup.object({
    firstName: Yup.string().required('First Name is required'),
    lastName: Yup.string().required('Last Name is required'),
    phoneNumber: Yup.string().required('Phone Number is required'),
    email: Yup.string().email('Email is invalid').required('Email is required'),
    dateOfBirth: Yup.string(),
    password: Yup.string()
      .min(6, 'Password must be at least 6 characters')
      .required('Password is required'),
    passwordConfirm: Yup.string()
      .oneOf([Yup.ref('password'), null], 'Passwords must match')
      .required('Confirm Password is required'),
    interest: Yup.array()
      .of(
        Yup.string().oneOf([
          'myth',
          'romance',
          'novel',
          'scifi',
          'fantasy',
          'biography',
          'drama',
          'nonfiction',
          'selfhelp',
          'poem',
          'school',
          'college',
          'university',
        ])
      )
      .min(1, 'Must contain atleast one interest')
      .required('interest is required'),
  })
    .noUnknown(true)
    .strict(),
});

exports.logValidationSchema = Yup.object({
  body: Yup.object({
    email: Yup.string().email('Email is invalid').required('Email is required'),
    password: Yup.string()
      .min(6, 'Password must be at least 6 characters')
      .required('Password is required'),
  })
    .noUnknown(true)
    .strict(),
});
