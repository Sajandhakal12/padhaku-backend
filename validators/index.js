const {
  regValidationSchema,
  logValidationSchema,
} = require('./auth.validator');

const { postUserBookValidation } = require('./userBook.validator');
const { bookSchemaValidation } = require('./books.validator');

const fileValidation = require('./file.validation');

module.exports = {
  regValidationSchema,
  logValidationSchema,
  postUserBookValidation,
  bookSchemaValidation,
  fileValidation,
};
