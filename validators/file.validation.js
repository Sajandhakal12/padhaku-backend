// meValidation.IS_IMAGE_VALID
const {
  FILEFORMATS: { IMAGE },
} = require('../configs/enums');

const IS_IMAGE_VALID = async (req, res, next) => {
  if (req.file) {
    const { file } = req;
    if (IMAGE.includes(file.mimetype)) {
      if (file.size > 20 * 1024 * 1024) {
        return res.status(403).json({ message: 'file size exceeded' });
      }
      return next();
    }
    return res.status(403).json({ message: 'invalid file' });
  }
  next();
};

const IS_MULTIPLE_IMAGE_VALID = async (req, res, next) => {
  if (req.files) {
    if (req.files.length > 3) {
      return res
        .status(400)
        .json({ message: 'Cannot send more than 3 images' });
    }
    const index = req.files.findIndex((file) => {
      if (IMAGE.includes(file.mimetype)) {
        if (file.size > 20 * 1024 * 1024) {
          return true;
        }
        return false;
      }
      return false;
    });
    if (index !== -1) {
      return res
        .status(403)
        .json({ message: 'file size exceeded or invalid file' });
    }
    return next();
  }
  return res
    .status(400)
    .json({ message: 'Book must contain atleast one image' });
};

module.exports = { IS_IMAGE_VALID, IS_MULTIPLE_IMAGE_VALID };
