const Yup = require('yup');

exports.createThreadValidation = Yup.object({
  body: Yup.object({
    title: Yup.string().required('Title is required'),
    body: Yup.string().required('Body is required'),
  })
    .noUnknown(true)
    .strict(),
});

exports.getAllThreadValidation = Yup.object({
  query: Yup.object({
    email: Yup.string().email('Email is invalid').required('Email is required'),
    password: Yup.string()
      .min(6, 'Password must be at least 6 characters')
      .required('Password is required'),
  })
    .noUnknown(true)
    .strict(),
});
