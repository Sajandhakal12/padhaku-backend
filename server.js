const mongoose = require('mongoose');

const { server } = require('./app');

const DB = process.env.MONGODB_URL.replace(
  '<password>',
  process.env.MONGODB_PASSWORD
);

mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => console.log('DB connection successful!'));

const port = process.env.PORT || 4000;

server.listen(port, () => {
  console.log('Listening at PORT: ', port);
});

process.on('uncaughtException', (err) => {
  console.log('Uncaught rejection....shutting down');
  console.log('yohohoho', err, err.message);

  process.exit(1);
});

process.on('SIGTERM', () => {
  console.log('👋 SIGTERM RECEIVED. Shutting down gracefully');
  server.close(() => {
    console.log('💥 Process terminated!');
  });
});
