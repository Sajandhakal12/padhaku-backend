const ID = require('../utils/uuid');
const bucket = require('../configs/firebase.config');

exports.uploadFile = async (filename) => {
  const metadata = {
    metadata: {
      // This line is very important. It's to create a download token.
      firebaseStorageDownloadTokens: ID.uuidFast(),
    },
    contentType: 'image/png',
    cacheControl: 'public, max-age=31536000',
  };

  // Uploads a local file to the bucket
  const res = await bucket.upload(filename, {
    // Support for HTTP requests made with `Accept-Encoding: gzip`
    gzip: true,
    metadata,
  });
  const {
    bucket: bucketName,
    name,
    metadata: { firebaseStorageDownloadTokens },
  } = res[1];

  return `https://firebasestorage.googleapis.com/v0/b/${bucketName}/o/${name}?alt=media&token=${firebaseStorageDownloadTokens}`;
};
