import pandas as pd
# from scipy import sparse;
# from sklearn.metrics.pairwise import cosine_similarity

ratings = pd.read_csv("ratings.csv")
movies = pd.read_csv("movies.csv")
ratings = pd.merge(movies,ratings).drop(['genres','timestamp'],axis=1)
# print(ratings.head())


user_ratings = ratings.pivot_table(index=["userId"],columns=["title"],values='rating')
user_ratings = user_ratings.dropna(thresh=10,axis=1).fillna(0)
# print(user_ratings)

item_similarity_df = user_ratings.corr(method='pearson')
# print(item_similarity_df.head(50))


def get_similar(movie_name,rating):
    similar_ratings = corrMatrix[movie_name]*(rating-2.5)
    similar_ratings = similar_ratings.sort_values(ascending=False)
    #print(type(similar_ratings))
    return similar_ratings


romantic_lover = [("(500) Days of Summer (2009)",5),("Alice in Wonderland (2010)",3),("Aliens (1986)",1),("2001: A Space Odyssey (1968)",2)]
similar_movies = pd.DataFrame()
for movie,rating in romantic_lover:
    similar_movies = similar_movies.append(get_similar(movie,rating),ignore_index = True)

print(similar_movies.head(10))