// const AppError = require('../../utils/AppError');

// const handleJWTError = () =>
//   new AppError('Invalid token. Please log in again', 401);

// const handleJWTExpiredError = () =>
//   new AppError('Token expires. Please log in again', 401);

const sendErrorDev = (err, res) => {
  res.status(err.statusCode).json({
    status: err.status,
    error: err,
    message: err.message,
    stack: err.stack,
  });
};

// const sendErrorProd = (err, res) => {
//   // operational or trusted errors: send it to client
//   if (err.isOperational) {
//     res.status(err.statusCode).json({
//       status: err.status,
//       message: err.message,
//     });
//     // programming error dont send to client
//   } else {
//     //   1) Log error
//     console.error('Error', err);

//     // 2) send generic message
//     res.status(500).json({
//       status: 'error',
//       message: 'Something went terribly wrong',
//     });
//   }
// };

// eslint-disable-next-line no-unused-vars
const globalErrorHandler = (err, req, res, next) => {
  console.log('reached');

  // eslint-disable-next-line no-param-reassign
  err.statusCode = err.statusCode || 500;
  // eslint-disable-next-line no-param-reassign
  err.status = err.status || 'error';
  if (process.env.NODE_ENV === 'development') {
    sendErrorDev(err, res);
  } else if (process.env.NODE_ENV === 'production') {
    sendErrorDev(err, res);
    // let error = { ...err };
    // if (error.name === 'JsonWebTokenError') error = handleJWTError();
    // if (error.name === 'TokenExpiredError') error = handleJWTExpiredError();
    // sendErrorProd(error, res);
  }
};

module.exports = globalErrorHandler;
