const mongoose = require('mongoose');
const { catchAsyncError } = require('../../utils');
const { DirectMessage } = require('../../models');
const { CheckQuery, getFacetPipeline, prepareResult } = require('../../utils');

const mongooseObjectId = mongoose.Types.ObjectId;

exports.getMessages = catchAsyncError(async (req, res) => {
  const { userId } = req.params;
  const currentUser = req.user._id.toHexString();
  console.log(userId, currentUser);

  const filter = {
    $or: [
      {
        fromUser: mongooseObjectId(currentUser),
        toUser: mongooseObjectId(userId),
      },
      {
        fromUser: mongooseObjectId(userId),
        toUser: mongooseObjectId(currentUser),
      },
    ],
  };
  const options = CheckQuery(req.query, ['sort', 'limit', 'page']);
  const sort = { createdAt: -1 };
  if (options.sort) {
    const parts = options.sort.split(':');
    sort.createdAt = parts[1] === 'desc' ? -1 : 1;
  }
  const data = await DirectMessage.aggregate([
    {
      $match: filter,
    },
    {
      $lookup: {
        from: 'directmessages',
        localField: 'reply',
        foreignField: '_id',
        as: 'replyMessage',
      },
    },
    { $unwind: { path: '$replyMessage', preserveNullAndEmptyArrays: true } },
    {
      $project: {
        _id: 0,
        id: '$_id',
        fromUser: 1,
        toUser: 1,
        isEdited: 1,
        type: 1,
        data: 1,
        file: 1,
        location: 1,
        reply: {
          id: '$reply',
          reply: '$replyMessage.data',
          type: '$replyMessage.type',
        },
        createdAt: 1,
        updatedAt: 1,
      },
    },
    {
      $sort: sort,
    },
    getFacetPipeline(options),
  ]).allowDiskUse(true);

  const result = prepareResult(data, options);
  res.send(result);
});

exports.getLoggedInUserMessages = catchAsyncError(async (req, res) => {
  const currentUser = req.user._id.toHexString();
  const options = CheckQuery(req.query, ['limit', 'page']);
  const data = await DirectMessage.aggregate([
    {
      $match: {
        $or: [
          {
            fromUser: mongooseObjectId(currentUser),
            toUser: { $ne: mongooseObjectId(currentUser) },
          },
          {
            toUser: mongooseObjectId(currentUser),
            fromUser: { $ne: mongooseObjectId(currentUser) },
          },
        ],
      },
    },
    {
      $project: {
        _id: 1,
        toUser: 1,
        fromUser: 1,
        data: 1,
        file: 1,
        type: 1,
        createdAt: 1,
        fromToUser: ['$fromUser', '$toUser'],
      },
    },
    {
      $unwind: '$fromToUser',
    },
    {
      $sort: {
        fromToUser: 1,
      },
    },

    {
      $group: {
        _id: '$_id',
        id: {
          $last: '$_id',
        },
        fromToUser: {
          $push: '$fromToUser',
        },
        fromUser: {
          $first: '$fromUser',
        },
        toUser: {
          $first: '$toUser',
        },
        data: {
          $first: '$data',
        },
        file: {
          $first: '$file',
        },
        type: {
          $first: '$type',
        },
        createdAt: {
          $first: '$createdAt',
        },
      },
    },
    {
      $sort: {
        createdAt: -1,
      },
    },
    {
      $group: {
        _id: '$fromToUser',
        id: {
          $first: '$id',
        },
        fromUser: {
          $first: '$fromUser',
        },
        toUser: {
          $first: '$toUser',
        },
        data: {
          $first: '$data',
        },
        file: {
          $first: '$file',
        },
        type: {
          $first: '$type',
        },
        createdAt: {
          $first: '$createdAt',
        },
      },
    },
    {
      $sort: {
        createdAt: -1,
      },
    },
    {
      $addFields: {
        sent: { $eq: ['$fromUser', mongooseObjectId(currentUser)] },
      },
    },
    {
      $addFields: {
        joinUser: {
          $cond: {
            if: { $eq: ['$sent', true] },
            then: '$toUser',
            else: '$fromUser',
          },
        },
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'joinUser',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
      $unwind: '$user',
    },
    {
      $project: {
        _id: 0,
        id: 1,
        data: 1,
        file: 1,
        type: 1,
        sent: 1,
        'user._id': '$user._id',
        'user.firstName': 1,
        'user.lastName': 1,
        'user.imageUrl': 1,
        createdAt: 1,
      },
    },
    getFacetPipeline(options),
  ]).allowDiskUse(true);

  const result = prepareResult(data, options);
  res.send(result);
});
