const { UserBook, Book } = require('../../models');
const { catchAsyncError, AppError, CheckQuery } = require('../../utils');
const factory = require('./factory.controller');
const { uploadFile } = require('../../services/firebase.service');

exports.uploadImageForBook = catchAsyncError(async (req, res, next) => {
  if (req.files) {
    console.log(req.files);
    const images = await Promise.all(
      req.files.map((file) => uploadFile(file.path))
    );
    console.log(images);
    req.body.images = images;
    return next();
  }
  next();
});

exports.addUserBook = factory.createOne(UserBook);
exports.getAllUserBooks = factory.getAll(UserBook);
exports.getAllUserBooksByCategory = catchAsyncError(async (req, res) => {
  const filters = req.query.isSold
    ? [{ $eq: ['$isSold', req.query.isSold] }]
    : [];
  const genrefilter = CheckQuery(req.params, ['genre']);
  const options = CheckQuery(req.query, ['page', 'limit', 'sort']);
  const sort = { createdAt: -1 };
  if (options.sortBy) {
    const parts = options.sortBy.split(':');
    sort.createdAt = parts[1] === 'desc' ? -1 : 1;
  }
  const userBooks = await Book.aggregate([
    { $match: genrefilter },
    {
      $lookup: {
        from: 'userbooks',
        let: { bookId: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ['$book', '$$bookId'], ...filters }],
              },
            },
          },
        ],
        as: 'userbook',
      },
    },
    { $match: { userbook: { $gt: { $size: 0 } } } },
    {
      $sort: sort,
    },
    {
      $facet: {
        // eslint-disable-next-line radix
        pagination: [
          { $count: 'total' },
          // eslint-disable-next-line radix
          { $addFields: { page: parseInt(options.page || 1) } },
        ],
        docs: [
          {
            $skip:
              // eslint-disable-next-line radix
              (parseInt(options.page || 1) - 1) * parseInt(options.limit || 10),
          },
          {
            // eslint-disable-next-line radix
            $limit: parseInt(options.limit || 10),
          },
        ],
      },
    },
  ]).allowDiskUse(true);

  const desiredDocs = userBooks[0].docs ? userBooks[0].docs : [];
  // // eslint-disable-next-line radix
  const pagination =
    userBooks[0].pagination && userBooks[0].pagination[0] !== undefined
      ? userBooks[0].pagination[0] // eslint-disable-next-line radix
      : { total: 0, page: parseInt(options.page || 1) };

  const result = {
    results: desiredDocs,
    page: pagination.page,
    limit: options.limit || 10,
    totalPages: Math.ceil(pagination.total / options.limit || 10),
    totalResults: pagination.total,
  };
  res.send(result);
});

exports.recommendBook = catchAsyncError(async (req, res) => {
  const options = CheckQuery(req.query, ['page', 'limit', 'sort']);
  const sort = { createdAt: -1 };
  if (options.sortBy) {
    const parts = options.sortBy.split(':');
    sort.createdAt = parts[1] === 'desc' ? -1 : 1;
  }
  const userBooks = await Book.aggregate([
    { $match: { genre: { $in: req.user.interest } } },
    {
      $lookup: {
        from: 'userbooks',
        let: { bookId: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ['$book', '$$bookId'] }],
              },
            },
          },
        ],
        as: 'userbook',
      },
    },
    { $match: { userbook: { $gt: { $size: 0 } } } },
    {
      $sort: sort,
    },
    {
      $facet: {
        // eslint-disable-next-line radix
        pagination: [
          { $count: 'total' },
          // eslint-disable-next-line radix
          { $addFields: { page: parseInt(options.page || 1) } },
        ],
        docs: [
          {
            $skip:
              // eslint-disable-next-line radix
              (parseInt(options.page || 1) - 1) * parseInt(options.limit || 10),
          },
          {
            // eslint-disable-next-line radix
            $limit: parseInt(options.limit || 10),
          },
        ],
      },
    },
  ]).allowDiskUse(true);

  const desiredDocs = userBooks[0].docs ? userBooks[0].docs : [];
  // // eslint-disable-next-line radix
  const pagination =
    userBooks[0].pagination && userBooks[0].pagination[0] !== undefined
      ? userBooks[0].pagination[0] // eslint-disable-next-line radix
      : { total: 0, page: parseInt(options.page || 1) };

  const result = {
    results: desiredDocs,
    page: pagination.page,
    limit: options.limit || 10,
    totalPages: Math.ceil(pagination.total / options.limit || 10),
    totalResults: pagination.total,
  };
  res.send(result);
});

exports.getOneBook = factory.getOne(UserBook);

exports.UpdateBookStatus = catchAsyncError(async (req, res, next) => {
  const userBook = await UserBook.findById(req.params.bookId);
  if (!userBook) {
    return next(new AppError('No document found with that ID', 404));
  }
  userBook.set({ isSold: true });
  await userBook.save();
  res.send();
});
