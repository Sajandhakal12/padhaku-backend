const { Book, Review } = require('../../models');
const { catchAsyncError } = require('../../utils');
const factory = require('./factory.controller');
const { uploadFile } = require('../../services/firebase.service');

exports.getAllBooks = factory.getAll(Book);
exports.getBookById = factory.getOne(Book);
exports.deleteBookById = factory.deleteOne(Book);
exports.updateBookById = factory.updateOne(Book);

exports.insertBook = catchAsyncError(async (req, res) => {
  const { file } = req;

  const { rating, review, ...rest } = req.body;
  if (file) {
    const imageUrl = await uploadFile(file.path);
    rest.image = imageUrl;
  }
  const doc = await Book.create(rest);
  if (rating || (rating && review)) {
    await Review.create({
      rating,
      review,
      book: doc._id,
      user: req.user._id,
    });
  }

  res.status(201).json({
    status: 'success',
    message: 'Book added successfully',
    data: doc,
  });
});

exports.preReview = catchAsyncError(async (req, res, next) => {
  req.body.book = req.params.bookId;
  req.body.user = req.user._id;
  next();
});

exports.createReview = factory.createOne(Review);
exports.updateReview = factory.updateOne(Review);

exports.getAllReview = catchAsyncError(async (req, res) => {
  const { bookId } = req.params;

  const books = await Review.find({ book: bookId }).select('-book').lean();
  res.status(200).json({
    status: 'success',
    data: books,
  });
});
