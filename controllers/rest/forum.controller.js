const mongoose = require('mongoose');
const { Thread, Comment, Like, Notification } = require('../../models');
const { catchAsyncError } = require('../../utils');
const factory = require('./factory.controller');

// posting a thread
exports.postOneThread = catchAsyncError(async (req, res) => {
  const newThread = {
    ...req.body,
    user: req.user._id,
  };

  const thread = await Thread.create(newThread);

  res.status(201).json({
    status: 'SUCCESS',
    code: 201,
    message: 'New Thread added',
    data: thread,
  });
});

// fetching all the threads
exports.getAllThreads = factory.getAll(Thread);

// get a thread
exports.getThread = catchAsyncError(async (req, res) => {
  const thread = await Thread.aggregate([
    { $match: { _id: mongoose.Types.ObjectId(req.params.threadId) } },
    {
      $lookup: {
        from: 'comments',
        let: { threadId: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ['$thread', '$$threadId'] }],
              },
            },
          },
          {
            $lookup: {
              from: 'users',
              let: { userId: '$user' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [{ $eq: ['$_id', '$$userId'] }],
                    },
                  },
                },
                {
                  $project: {
                    firstName: 1,
                    lastName: 1,
                    imageUrl: 1,
                  },
                },
              ],
              as: 'user',
            },
          },
          { $unwind: { path: '$user' } },
        ],
        as: 'comments',
      },
    },
    {
      $lookup: {
        from: 'likes',
        let: { threadId: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [{ $eq: ['$thread', '$$threadId'] }],
              },
            },
          },
        ],
        as: 'likes',
      },
    },
  ]);
  if (thread.length === 0) {
    return res.status(404).json({ error: 'thread not found' });
  }
  res.status(200).json({ status: 'SUCCESS', code: 200, thread: thread.pop() });
});

// comment on a Thread
exports.commentOnThread = catchAsyncError(async (req, res) => {
  const thread = await Thread.findById(req.params.threadId).select(
    'commentCount user'
  );

  if (!thread) {
    return res.status(404).json('no such thread');
  }

  // add a comment in comment table
  await Comment.create({ thread: thread._id, user: req.user._id, ...req.body });

  const notification = {
    recipient: thread.user,
    sender: req.user._id,
    type: 'comment',
    in: 'Thread',
    seen: false,
    thread: thread._id,
  };
  // create notification on comment
  await Notification.create(notification);

  res.status(201).json({
    status: 'SUCCESS',
    code: 201,
    message: 'New Comment added',
    data: thread,
  });
});

// // like a thread
exports.likeThread = catchAsyncError(async (req, res) => {
  const like = {
    user: req.user._id,
    thread: req.params.threadId,
  };

  const thread = await Thread.findById(like.thread).select('');

  if (!thread) {
    return res.status(404).json({ error: 'no such thread' });
  }

  const likedThread = await Like.findOne(like).lean();

  if (likedThread) {
    res.status(400).json({ error: 'Thread Already liked' });
  }
  await Like.create(like);

  const notification = {
    recipient: thread.user,
    sender: req.user._id,
    type: 'like',
    in: 'Thread',
    seen: false,
    thread: thread._id,
  };
  // create notification on comment
  await Notification.create(notification);

  res.send();
});

// // unlike a thread
exports.unlikeThread = catchAsyncError(async (req, res) => {
  const like = {
    user: req.user._id,
    thread: req.params.threadId,
  };

  const thread = await Thread.findById(like.thread).select('');

  if (!thread) {
    return res.status(404).json({ error: 'no such thread' });
  }

  const likedThread = await Like.findOne(like).lean();

  if (!likedThread) {
    res.status(400).json({ error: 'Thread not liked' });
  }
  await Like.deleteOne(like);

  res.send();

  //       // remind me to shift this upward
  //       await ExecuteQuery(
  //         updateByField,
  //         'threads',
  //         { likeCount: threadData.likeCount - 1 },
  //         'id',
  //         like.threadId
  //       );
  //       threadData.likeCount -= 1;
  //       await ExecuteQuery(
  //         deleteByFourField,
  //         'notifications',
  //         'sender',
  //         req.user.userId,
  //         'recipient',
  //         threadData.userId,
  //         'threadId',
  //         threadData.id,
  //         'type',
  //         'like'
  //       );
});

// // todo notification on comment post
// // deleting a thread
exports.deleteThread = catchAsyncError(async (req, res) => {
  await Thread.deleteMany({ _id: req.params.threadId });
  //   await ExecuteQuery(deleteByField, 'likes', 'threadId', req.params.threadId);
  //   await ExecuteQuery(
  //     deleteByField,
  //     'comments',
  //     'threadId',
  //     req.params.threadId
  //   );
  //   await ExecuteQuery(
  //     deleteByField,
  //     'notifications',
  //     'threadId',
  //     req.params.threadId
  //   );
  res.status(200).json({ message: 'success' });
});
