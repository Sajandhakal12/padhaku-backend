const { User, Notification } = require('../../models');
const { catchAsyncError, AppError } = require('../../utils');
const factory = require('./factory.controller');
const { uploadFile } = require('../../services/firebase.service');

const filterObj = (obj, ...allowedFields) => {
  const newObj = {};
  Object.keys(obj).forEach((el) => {
    if (allowedFields.includes(el)) newObj[el] = obj[el];
  });
  return newObj;
};

exports.getMe = (req, res, next) => {
  req.params.id = req.user._id;
  next();
};

exports.updateMe = catchAsyncError(async (req, res, next) => {
  // 1) Create error if user POSTs password data
  if (req.body.password || req.body.passwordConfirm) {
    return next(
      new AppError(
        'This route is not for password updates. Please use /updateMyPassword.',
        400
      )
    );
  }

  // 2) Filtered out unwanted fields names that are not allowed to be updated
  const filteredBody = filterObj(req.body, 'name', 'email');

  // 3) Update user document
  const updatedUser = await User.findByIdAndUpdate(req.user._id, filteredBody, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    status: 'success',
    data: {
      user: updatedUser,
    },
  });
});

exports.updateLikedProductMe = catchAsyncError(async (req, res) => {
  await User.updateOne(
    { _id: req.user._id },
    { $addToSet: { likedProduct: req.body.likedProduct } }
  );
  res.send();
});

exports.deleteMe = catchAsyncError(async (req, res) => {
  await User.findByIdAndUpdate(req.user._id, { active: false });

  res.status(204).json({
    status: 'success',
    data: null,
  });
});

exports.createUser = (req, res) => {
  res.status(500).json({
    status: 'error',
    message: 'This route is not defined! Please use /signup instead',
  });
};

exports.getUser = factory.getOne(User);
exports.getAllUsers = factory.getAll(User);

// Do NOT update passwords with this!
exports.updateUser = factory.updateOne(User);
exports.deleteUser = factory.deleteOne(User);

exports.uploadUserAvatar = catchAsyncError(async (req, res) => {
  const { file } = req;
  if (file) {
    try {
      const imageUrl = await uploadFile(file.path);
      const user = await User.findByIdAndUpdate(
        req.user._id,
        { imageUrl },
        {
          new: true,
          runValidators: true,
        }
      );
      res.send(user);
    } catch (e) {
      console.log(e);
    }
  }
});

exports.getAllUserNotification = factory.getAll(Notification);
