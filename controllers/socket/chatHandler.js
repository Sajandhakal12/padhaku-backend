// ChatHandler class for handling all socket-io events

class ChatHandler {
  /**
   *
   * @param client
   * @param clientManager
   * @param chatroomManager
   * @param io
   */
  constructor(client, directMessageHandler, clientManager, io) {
    console.log(clientManager);
    this.client = client;
    this.clientManager = clientManager;
    this.directMessageHandler = directMessageHandler;
    this.io = io;
  }

  async handleDirectMessageInternal(userId, message) {
    console.log(userId, this.client.decoded.id, message);
    const toUser = await this.clientManager.getUserByUserId(userId);

    // append event to chat history
    const data = await this.directMessageHandler.addEntry({
      fromUser: this.client.decoded.id,
      toUser: userId,
      message,
    });

    // notify other clients in chatroom
    if (toUser._id) {
      this.directMessageHandler.sendMessage(
        toUser._id.toHexString(),
        {
          isDirectMessage: true,
          fromUser: this.client.decoded.id,
          toUser: userId,
          type: 'message',
          data: message,
          createdAt: data.createdAt,
          updatedAt: data.updatedAt,
        },
        this.io
      );
    }

    this.directMessageHandler.sendMessage(
      this.client.id,
      {
        isDirectMessage: true,
        fromUser: this.client.decoded.id,
        toUser: userId,
        data: message,
        type: 'message',
        createdAt: data.createdAt,
        updatedAt: data.updatedAt,
      },
      this.io
    );

    return data;
  }

  async handleMessage({ userId, message } = {}, callback) {
    try {
      if (userId) {
        await this.handleDirectMessageInternal(userId, message);
      }

      callback(null);
    } catch (err) {
      callback(err);
    }
  }

  async handleDisconnect() {
    // remove user profile
    this.clientManager.removeClient(this.client);
  }
}

module.exports = ChatHandler;
