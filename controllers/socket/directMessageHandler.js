// Class for managing Chatroom messages and users

const { DirectMessage } = require('../../models');

class DirectMessageHandler {
  /**
   * send message to a client
   * @param message
   */
  sendMessage(clientId, message, io) {
    io.to(clientId).emit('message', message);
  }

  /**
   * Add message to chat history
   * @param entry: {fromUser, toUser, message}
   */
  async addEntry(entry) {
    const data = {
      fromUser: entry.fromUser,
      toUser: entry.toUser,
      data: entry.message,
    };

    if (entry.reply) {
      data.type = 'reply';
    } else data.type = 'message';

    const directMessage = new DirectMessage(data);
    await directMessage.save();
    return directMessage;
  }

  /**
   * return chat room history for new users
   * @returns {*[]}
   */
  async getChatHistoryForUser(currentUser, targetUser) {
    const directMessages = await DirectMessage.find()
      .or([
        { fromUser: currentUser, toUser: targetUser },
        { fromUser: targetUser, toUser: currentUser },
      ])
      .populate(['fromUser', 'toUser'])
      .sort({ createdAt: -1 })
      .limit(10)
      .lean();

    return directMessages;
  }

  serialize() {
    return {
      ...this.info,
    };
  }
}

module.exports = DirectMessageHandler;
