// Client for managing all users
const { User } = require('../../models');

class ClientManager {
  async addClient(client) {
    client.join(client.decoded.id);
  }

  removeClient(client) {
    client.leave(client.decoded.id);
  }

  async getUserByUserId(userId) {
    console.log(userId);
    return User.findById(userId);
  }
}

module.exports = ClientManager;
