const express = require('express');
const morgan = require('morgan');
const path = require('path');

// const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
// const xss = require('xss-clean');
const hpp = require('hpp');
const cors = require('cors');
require('dotenv').config();
const cookieParser = require('cookie-parser');
const mongoSanitize = require('express-mongo-sanitize');
const { promisify } = require('util');
const jwt = require('jsonwebtoken');

const ClientManager = require('./controllers/socket/clientManager');
const ChatHandler = require('./controllers/socket/chatHandler');
const DirectMessageHandler = require('./controllers/socket/directMessageHandler');
const AppError = require('./utils/AppError');
const globalErrorHandler = require('./controllers/rest/error.controller');
const { app, server, socketio: io } = require('./express');
const routes = require('./routes/v1');

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));

// security http headers
app.use(helmet());

const corsOptionsDelegate = function (req, callback) {
  let corsOptions;
  const allowlist = [
    'http://localhost:3000',
    'https://localhost:3000',
    'https://padhaku.netlify.app',
  ];
  if (allowlist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { origin: true, credentials: true };
  } else {
    corsOptions = { origin: false, credentials: true };
  }
  callback(null, corsOptions);
};

app.use(cors(corsOptionsDelegate));

const clientManager = new ClientManager();
const directMessageHandler = new DirectMessageHandler();

const wrap = (middleware) => (socket, next) =>
  middleware(socket.handshake, {}, next);

io.use(wrap(cookieParser()));

io.use(async function (socket, next) {
  if (socket.handshake.cookies && socket.handshake.cookies.jwt) {
    try {
      const decoded = await promisify(jwt.verify)(
        socket.handshake.cookies.jwt,
        process.env.JWT_SECRET
      );

      // eslint-disable-next-line
      socket.decoded = decoded;
      next();
    } catch (err) {
      console.log(err);
      return next(new Error('Authentication error - invalid token'));
    }
  } else {
    next(new Error('Authentication error - token is required'));
  }
}).on('connection', (client) => {
  const chatHandler = new ChatHandler(
    client,
    directMessageHandler,
    clientManager,
    io
  );

  console.log('client connected...', client.decoded, client.id);
  clientManager.addClient(client);

  client.on('message', chatHandler.handleMessage.bind(chatHandler));

  client.on('disconnect', () => {
    console.log('client disconnect...', client.id);
    chatHandler.handleDisconnect.bind(chatHandler)();
  });

  client.on('error', (err) => {
    console.log('received error from client:', client.id);
    console.log(err);
  });
});

// limiting the reqpuset from same ip
// const limiter = rateLimit({
//   max: 100,
//   windowMs: 10 * 60 * 1000,
//   message: 'Too many requests from this IP. Please try again in an hour',
// });

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(express.json({ limit: '10kb' }));

app.use(cookieParser());

// Data sanitization against NoSQL query injection
app.use(mongoSanitize());

// Data sanitization against xss
// app.use(xss());

// prevent parameter pollution
app.use(
  hpp({
    whitelist: ['page', 'limit', 'searchText'],
  })
);
// only applies to route starting with /api
// app.use('/api/v1', limiter);

// routes
app.use('/api/v1', routes);

app.use('*', (req, res, next) => {
  next(new AppError(`can't find ${req.originalUrl} on the server`, 404));
});

app.use(globalErrorHandler);

module.exports = {
  server,
};
