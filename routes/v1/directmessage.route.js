const express = require('express');

// const { validateDTO } = require('../../utils/validator');
const {
  getMessages,
  getLoggedInUserMessages,
} = require('../../controllers/rest/directMessage.controller');
const { verifyToken } = require('../../controllers/rest/auth.controller');

const router = express.Router();
router.get('/', verifyToken, getLoggedInUserMessages);
router.get('/:userId', verifyToken, getMessages);

module.exports = router;
