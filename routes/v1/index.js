const express = require('express');

const authRoute = require('./auth.route');
const userRotue = require('./users.route');
const threadRoute = require('./threads.route');
const directMessageRoute = require('./directmessage.route');
const bookRoute = require('./books.route');
const useBooksRoute = require('./userbook.route');

const router = express.Router();

router.use('/auth', authRoute);
router.use('/users', userRotue);
router.use('/threads', threadRoute);
router.use('/directMessage', directMessageRoute);
router.use('/book', bookRoute);
router.use('/userBook', useBooksRoute);

module.exports = router;
