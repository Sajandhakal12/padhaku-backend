const express = require('express');

const { verifyToken } = require('../../controllers/rest/auth.controller');
const upload = require('../../configs/multer.config');

const {
  insertBook,
  getAllBooks,
  getBookById,
  deleteBookById,
  updateBookById,
  preReview,
  createReview,
  updateReview,
  getAllReview,
} = require('../../controllers/rest/book.controller');

const { bookSchemaValidation, fileValidation } = require('../../validators');
const { validateDTO } = require('../../middleware/validator');

const router = express.Router();

// Book Route
router
  .route('/')
  .post(
    verifyToken,
    upload.single('image'),
    fileValidation.IS_IMAGE_VALID,
    (req, res, next) => {
      try {
        const genre = JSON.parse(req.body.genre);
        req.body.genre = genre;
        next();
      } catch (e) {
        console.log(e);
        next();
      }
    },
    validateDTO(bookSchemaValidation),
    insertBook
  )
  .get(getAllBooks);
router
  .route('/:id')
  .get(getBookById)
  .put(verifyToken, updateBookById)
  .delete(verifyToken, deleteBookById);

router
  .route('/:bookId/review')
  .post(verifyToken, preReview, createReview)
  .get(getAllReview);
router.route('/:bookId/review/:id').put(verifyToken, preReview, updateReview);

module.exports = router;
