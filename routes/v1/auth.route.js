const express = require('express');

const {
  logValidationSchema,
  regValidationSchema,
} = require('../../validators');

const { validateDTO } = require('../../middleware/validator');

const {
  signUpUser,
  loginUser,
  forgetPassword,
  resetPassword,
  signoutUser,
} = require('../../controllers/rest/auth.controller');

const router = express.Router();
router.post('/register', validateDTO(regValidationSchema), signUpUser);
router.post('/login', validateDTO(logValidationSchema), loginUser);
router.post('/forgetPassword', forgetPassword);
router.patch('/resetPassword/:token', resetPassword);
router.post('/logout', signoutUser);

module.exports = router;
