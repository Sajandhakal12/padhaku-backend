const express = require('express');
const upload = require('../../configs/multer.config');

const { verifyToken } = require('../../controllers/rest/auth.controller');
const {
  uploadImageForBook,
  addUserBook,
  getAllUserBooks,
  getOneBook,
  getAllUserBooksByCategory,
  UpdateBookStatus,
  recommendBook,
} = require('../../controllers/rest/userBook.controller');
const { postUserBookValidation, fileValidation } = require('../../validators');

const { validateDTO } = require('../../middleware/validator');

const router = express.Router();

router.post(
  '/add',
  verifyToken,
  upload.any(),
  fileValidation.IS_MULTIPLE_IMAGE_VALID,
  validateDTO(postUserBookValidation),
  uploadImageForBook,
  addUserBook
);
router.get('/', getAllUserBooks);
router.get('/:id', getOneBook);
router.get('/category/:genre', getAllUserBooksByCategory);
router.get('/recommend/books', verifyToken, recommendBook);
router.put('/:bookId/sold', verifyToken, UpdateBookStatus);

// UpdateBookStatus

module.exports = router;
