const express = require('express');

const {
  postOneThread,
  getAllThreads,
  deleteThread,
  commentOnThread,
  getThread,
  likeThread,
  unlikeThread,
} = require('../../controllers/rest/forum.controller');
const { verifyToken } = require('../../controllers/rest/auth.controller');

const router = express.Router();

// Thread Routes
router.route('/').post(verifyToken, postOneThread).get(getAllThreads);
router.route('/:threadId').get(getThread).delete(verifyToken, deleteThread);
router.use(verifyToken);
router.post('/:threadId/comment', commentOnThread);
router.post('/:threadId/like', likeThread);
router.post('/:threadId/unlike', unlikeThread);

module.exports = router;
