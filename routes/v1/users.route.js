const express = require('express');

const {
  getMe,
  updateMe,
  deleteMe,
  createUser,
  getUser,
  getAllUsers,
  updateUser,
  deleteUser,
  uploadUserAvatar,
  updateLikedProductMe,
  getAllUserNotification,
} = require('../../controllers/rest/user.controller');

const upload = require('../../configs/multer.config');

const { verifyToken } = require('../../controllers/rest/auth.controller');

const router = express.Router();

// user Routes
router.use(verifyToken);
router.route('/').post(createUser).get(getAllUsers);
router.route('/me').get(getMe, getUser).put(updateMe).delete(deleteMe);
router.route('/me/likedProduct').put(updateLikedProductMe);
router.route('/me/upload').post(upload.single('avatar'), uploadUserAvatar);
router.route('/:id').get(getUser).put(updateUser).delete(deleteUser);
router.route('/me/notification').get(getAllUserNotification);

// router.post('/notifications', markNotificationsRead);

module.exports = router;
