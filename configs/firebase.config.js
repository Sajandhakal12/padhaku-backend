const admin = require('firebase-admin');

// CHANGE: The path to your service account
const serviceAccount = require('./serviceAccountKey.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: 'padhaku-bd3c2.appspot.com',
});

const bucket = admin.storage().bucket();

module.exports = bucket;
