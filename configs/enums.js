const enums = Object.freeze({
  MESSAGETYPE: {
    ENUM: ['message', 'file'],
    DEFAULT: 'message',
  },
  FILEFORMATS: {
    VIDEO: ['video/mp4', 'video/webm', 'video/3gpp', 'video/ogg'],
    AUDIO: [
      'audio/3gpp2',
      'audio/3gpp',
      'audio/webm',
      'audio/ogg',
      'audio/mpeg',
    ],
    DOCS: [
      'application/pdf',
      'application/msword',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'application/vnd.ms-powerpoint',
      'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      'application/vnd.ms-excel',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    ],
    IMAGE: ['image/jpg', 'image/jpeg', 'image/png', 'image/webp'],
  },
});

module.exports = enums;
