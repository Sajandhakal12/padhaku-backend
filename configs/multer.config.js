const multer = require('multer');

// const storage = multer.memoryStorage();
const storage = multer.diskStorage({
  filename: (req, file, cb) => {
    cb(null, Date.now() + file.originalname);
  },
});

// limit of 100 mb
const upload = multer({ storage, limits: { fileSize: 100 * 1024 * 1024 } });

// const storage = multer.memoryStorage();
// const upload = multer({ storage });
module.exports = upload;
