const socketio = require('socket.io');
const { createServer } = require('http');
const express = require('express');

const app = express();
const server = createServer(app);
const io = socketio(server, {
  cors: {
    origin: (origin, fn) => {
      const allowlist = [
        'http://localhost:3000',
        'https://padhaku.netlify.app',
      ];
      if (allowlist.indexOf(origin) !== -1) return fn(null, origin);
      return fn('Error Invalid domain');
    },
    methods: ['GET', 'POST'],
    credentials: true,
  },

  allowEIO3: true, // false by default
});

module.exports = {
  app,
  server,
  socketio: io,
};
