const { CheckQuery } = require('../utils');

exports.validateDTO = (schema) => {
  return async (req, res, next) => {
    try {
      const options = CheckQuery(req, ['body', 'params', 'query']);
      console.log(options);
      const validatedOption = await schema.validate(options);
      if (validatedOption.body) req.body = validatedOption.body;
      if (validatedOption.params) req.params = validatedOption.params;
      if (validatedOption.query) req.query = validatedOption.query;
      next();
    } catch (e) {
      console.log('error yoyo');
      res.status(400).json(e);
    }
  };
};
